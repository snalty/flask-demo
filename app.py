from flask import Flask, render_template

app = Flask(__name__)
@app.route('/')
def hello_world():
 name = 'Sam'
 return render_template('template.html.j2', name=name)
